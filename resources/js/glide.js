let el = document.querySelector('.glide')
el.style.display = 'none';
document.querySelector('.generatedCarousel').onclick = () => {
 
  if (el.style.display === '') {
    Glide('.glide').destroy();
  } else {
    el.style.display = '';
    new Glide('.glide').mount();
  }
}
