let express = require('express'),
    router = express.Router(),
    assert = require('assert') 

router.get('/', (req, res) => {
  res.render('index')  
})

router.get('/carousel', (req, res) => {
  res.render('carousel')  
})

router.get('/bpm', (req, res) => {
  res.render('bpm')  
})

router.get('/examples', (req, res) => {
  res.render('examples')
})

module.exports = router;