var gulp = require('gulp');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');


/* Task to compile less */
gulp.task('compile-less', function () {
	gulp.src('./resources/less/style.less')
		.pipe(less())
		.pipe(gulp.dest('./resources/css'));
});
/* Task to watch less changes */
gulp.task('watch', function () {
	gulp.watch('./resources/**/*.less', ['compile-less']);
	gulp.watch("./resources/*.less").on("change", reload);
	gulp.watch("./views/*.html").on("change", reload);
});



gulp.task('browser-sync', ['nodemon'], function () {
	browserSync.init(null, {
		proxy: "http://localhost:3000",
		files: ["resources/**/*.*"],
		port: 3000,
	});
});
gulp.task('nodemon', function (cb) {

	var started = false;

	return nodemon({
		script: 'server.js'
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb();
			started = true;


		}
	});
});


/* Task when running `gulp` from terminal */
gulp.task('default', ['browser-sync', 'watch'], function () {});